#ifndef IMAGE_HANDLER
#define IMAGE_HANDLER

#include "image_structure.h"
#include <stddef.h>
#include <stdio.h>

struct image image_create(size_t width, size_t height);

void image_free(struct image* img);

#endif
