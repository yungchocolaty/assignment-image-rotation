#ifndef BMP_HANDLER
#define BMP_HANDLER

#include "image_structure.h"
#include "bmp_file_reader.h"
#include "bmp_file_writer.h"
#include <stdbool.h>
#include <stdio.h>

bool from_bmp(struct image* img, char* filename);

bool to_bmp(struct image* img, char* filename);

#endif
