#ifndef BMP_FILE_READER
#define BMP_FILE_READER

#include "image_handler.h"
#include "image_structure.h"
#include "utils.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>


enum read_status  {
  READ_OK = 0,
  READ_INVALID_BODY,
  READ_INVALID_HEADER,
  READ_INVALID_ARGUMENTS
};

enum read_status read_bmp(FILE* file, struct image* img);

#endif
