#ifndef BMP_FILE_WRITER
#define BMP_FILE_WRITER

#include "image_structure.h"
#include "utils.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

enum write_status  {
  WRITE_OK = 0,
  WRITE_INVALID_HEADER,
  WRITE_INVALID_BODY,
  WRITE_INVALID_ARGUMENTS
};

enum write_status write_bmp(FILE* file, struct image* img);

#endif
