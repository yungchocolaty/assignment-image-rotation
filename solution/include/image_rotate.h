#ifndef IMAGE_ROTATE
#define IMAGE_ROTATE

#include "image_handler.h"
#include <stdio.h>

struct image rotate_left(struct image const* img);

#endif
