#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>
#include "image_structure.h"

uint8_t count_padding(uint64_t width);

#endif
