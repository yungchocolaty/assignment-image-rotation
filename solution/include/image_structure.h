#ifndef IMAGE_STRUCTURE
#define IMAGE_STRUCTURE

#include <inttypes.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
  size_t width, height;
  struct pixel* data;
}__attribute__((packed));

#endif
