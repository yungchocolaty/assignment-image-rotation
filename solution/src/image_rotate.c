#include "image_rotate.h"

struct image rotate_left(struct image const* img) {
    if (img == NULL) {
        fprintf(stderr, "Argument(s) is NULL");
        return (struct image) {0};
    }

    struct image new_img = image_create(img->height, img->width);
    if (new_img.data == NULL) {
        fprintf(stderr, "rotate_left: image_create failed");
        return (struct image) {0};
    }

    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            new_img.data[img->height*j + img->height-i-1] = img->data[img->width*i + j];
        }
    }
    return new_img;
}
