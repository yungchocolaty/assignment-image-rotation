#include "bmp_handler.h"

bool from_bmp(struct image* img, char* filename) {
    FILE* input = fopen(filename, "r");
    if (input == NULL) {
        fprintf(stderr, "%s opening failed", filename);
        return false;
    }

    enum read_status r_status = read_bmp(input, img);
    if (r_status != READ_OK) {
        fclose(input);
        fprintf(stderr, "read_bmp failed\n");
        return false;
    }

    return true;
}

bool to_bmp(struct image* img, char* filename) {
    FILE* output = fopen(filename, "w");
    if (output == NULL) {
        fprintf(stderr, "%s opening failed", filename);
        return false;
    }

    enum write_status w_status = write_bmp(output, img);
    if (w_status != WRITE_OK) {
        fclose(output);
        fprintf(stderr, "write_bmp failed\n");
        return false;
    }

    return true;
}
