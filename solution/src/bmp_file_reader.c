#include "bmp_file_reader.h"

struct bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
}__attribute__((packed));

static bool read_bmp_header(FILE* file, struct bmp_header* header) {
    if (file == NULL || header == NULL) {
        fprintf(stderr, "Argument(s) is NULL");
        return false;
    }
    const size_t status = fread(header, sizeof(struct bmp_header), 1, file);
    return status == 1;
}

static bool read_bmp_body(FILE* file, struct image* img, uint64_t width, uint64_t height) {
    if (file == NULL || img == NULL) {
        fprintf(stderr, "Argument(s) is NULL");
        return false;
    }

    *img = image_create(width, height);

    const uint8_t padding = count_padding(img->width);
    for (size_t row = 0; row < height; row++) {
        for (size_t col = 0; col < width; col++) {
            fread(&(img->data[row * width + col].b), sizeof(uint8_t), 1, file);
            fread(&(img->data[row * width + col].g), sizeof(uint8_t), 1, file);
            fread(&(img->data[row * width + col].r), sizeof(uint8_t), 1, file);
        }
        fseek(file, padding, SEEK_CUR);
    }

    return true;
}

enum read_status read_bmp(FILE* file, struct image* img) {
    if (file == NULL || img == NULL) {
        return READ_INVALID_ARGUMENTS;
    }

    struct bmp_header header = {0};

    if (!read_bmp_header(file, &header) || header.biBitCount != 24) {
        fprintf(stderr, "read_bmp_header failed");
        return READ_INVALID_HEADER;
    }

    if (!read_bmp_body(file, img, header.biWidth, header.biHeight)) {
        fprintf(stderr, "read_bmp_body failed");
        return READ_INVALID_BODY;
    }

    return READ_OK;
}
