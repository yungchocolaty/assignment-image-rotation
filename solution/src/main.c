#include "bmp_file_reader.h"
#include "bmp_file_writer.h"
#include "bmp_handler.h"
#include "image_handler.h"
#include "image_rotate.h"
#include "image_structure.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if (argc != 3) {
        fprintf(stderr, "there must be 2 arguments\n");
        return 1;
    }

    struct image img = {0};
    if (!from_bmp(&img, argv[1])) {
        image_free(&img);
        return 1;
    }

    struct image new_img = rotate_left(&img);
    if (new_img.data == NULL) {
        image_free(&img);
        return 1;
    }

    if (!to_bmp(&new_img, argv[2])) {
        image_free(&img);
        image_free(&new_img);
        return 1;
    }

    image_free(&img);
    image_free(&new_img);

    return 0;
}

