#include "image_handler.h"

struct image image_create(size_t width, size_t height) {
    struct image img = {0};
    img.data = malloc(width * height * sizeof(struct pixel));
    if (img.data == NULL) {
        fprintf(stderr, "image_create: malloc failed");
        return (struct image) {0};
    }
    img.width = width;
    img.height = height;

    return img;
}

void image_free(struct image* img) {
    free(img->data);
}
