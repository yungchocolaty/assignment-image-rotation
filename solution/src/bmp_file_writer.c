#include "bmp_file_writer.h"

struct bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
}__attribute__((packed));

static bool write_bmp_header(FILE* file, struct bmp_header* header) {
    if (file == NULL || header == NULL) {
        fprintf(stderr, "Argument(s) is NULL");
        return false;
    }
    const size_t status = fwrite(header, sizeof(struct bmp_header), 1, file);
    return status == 1;
}

static bool write_bmp_body(FILE* file, struct image* img, uint64_t width, uint64_t height) {
    if (file == NULL || img == NULL) {
        fprintf(stderr, "Argument(s) is NULL");
        return false;
    }

    const uint8_t padding = count_padding(img->width);
    for (size_t row = 0; row < height; row++) {
        for (size_t col = 0; col < width; col++) {
            fwrite(&(img->data[row * width + col]), sizeof(img->data[0]), 1, file);
        }
        uint8_t trash = 0;
        for (int i = 0; i < padding; i++) {
            fwrite(&trash, sizeof(uint8_t), 1, file);
        }
    }

    return true;
}

#define H_TYPE 19778
#define H_RESERVED 0
#define H_SIZE 40
#define H_PLANES 1
#define H_BITCOUNT 24
#define H_COMPRESSION 0
#define H_XPPM 0
#define H_YPPM 0
#define H_CLR_USED 0
#define H_CLR_IMPORTANT 0

static struct bmp_header bmp_header_create(uint32_t image_width, uint32_t image_heigth) {
    const uint32_t bmp_header_size = sizeof (struct bmp_header);
    const uint32_t bmp_image_size = (image_width * sizeof(struct pixel) + count_padding(image_width)) * image_heigth;

    return (struct bmp_header) {
        .bfType = H_TYPE,
        .bfReserved = H_RESERVED,
        .bOffBits = bmp_header_size,
        .biSize = H_SIZE,
        .biWidth = image_width,
        .biHeight = image_heigth,
        .biPlanes = H_PLANES,
        .biBitCount = H_BITCOUNT,
        .biCompression = H_COMPRESSION,
        .biSizeImage = bmp_image_size,
        .bfileSize = bmp_image_size + bmp_header_size,
        .biXPelsPerMeter = H_XPPM,
        .biYPelsPerMeter = H_YPPM,
        .biClrUsed = H_CLR_USED,
        .biClrImportant = H_CLR_IMPORTANT,
    };
}

enum write_status write_bmp(FILE* file, struct image* img) {
    if (file == NULL || img == NULL) {
        return WRITE_INVALID_ARGUMENTS;
    }

    struct bmp_header header = bmp_header_create(img->width, img->height);

    if (!write_bmp_header(file, &header)) {
        fprintf(stderr, "write_bmp_header failed");
        return WRITE_INVALID_HEADER;
    }

    if (!write_bmp_body(file, img, header.biWidth, header.biHeight)) {
        fprintf(stderr, "read_bmp_body failed");
        return WRITE_INVALID_BODY;
    }

    return WRITE_OK;
}
