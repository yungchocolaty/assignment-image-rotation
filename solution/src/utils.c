#include "utils.h"

#define ALIGNMENT 4

uint8_t count_padding(uint64_t width) {
    return ALIGNMENT - (width * sizeof(struct pixel)) % ALIGNMENT;
}
